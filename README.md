# ResponsiveApi

Esse projeto foi criado utilizando os frameworks Spring (Java) e Angular (Typescript).

## Dependências

Para o correto funcionamento deste, é necessário a instalação das seguintes dependências:
* [Java JDK 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Node.js >= 6](https://nodejs.org/en/)
* [Angular CLI](https://cli.angular.io/)

## Passo a Passo para executar o projeto

* Clonar o repositório em uma pasta de sua preferência.
* Realizar o `checkout` do branch `master`.
* Através do Console, acesse a pasta `front` e execute o comando `npm i` para realizar o download das dependências do frontend. Este comando pode demorar um pouco.
* Depois executar o comando `ng build --base-href /olx/ --output-path ..\src\main\resources\static`. Esse comando realizará  o build do frontend diretamente na pasta de arquivos estaticos.
* Retornar para a pasta raiz do projeto.
* Executar o comando `gradlew bootRun` e em alguns instântes o projeto estará rodando.

## Observações

O frontend do ResponsiveApi foi criado utilizando o Angular CLI. Caso precise de alguma ajuda, pode obter executando o comando `ng help` na pasta front ou acesse o [README do Angular CLI](https://github.com/angular/angular-cli/blob/master/README.md).

### Possíveis problemas

O Gradle Wrapper pode apresentar alguns problemas de execução em certos ambientes. Aconselha-se, nesses casos, realizar a instalação a parte do Gradle através do [link](https://gradle.org/install/).