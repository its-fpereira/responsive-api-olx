import { ResponsiveApiPage } from './app.po';

describe('responsive-api App', () => {
  let page: ResponsiveApiPage;

  beforeEach(() => {
    page = new ResponsiveApiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
