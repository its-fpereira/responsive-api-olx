import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

export function asObservable<T>(subject: Subject<T>) {
  return new Observable(fn => subject.subscribe(fn));
}
