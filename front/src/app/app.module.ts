import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {AdsModule} from './ads/ads.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpModule,
    SharedModule,
    AdsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
