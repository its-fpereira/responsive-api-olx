import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <app-header></app-header>
      <app-breadcrumb class="d-none d-md-block"></app-breadcrumb>
      <app-banner class="d-none d-md-block"></app-banner>
      <app-search-result></app-search-result>
    </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
