import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './header/header.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {BannerComponent} from './banner/banner.component';
import {LocaleFilterComponent} from './locale-filter/locale-filter.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule
  ],
  declarations: [HeaderComponent, BreadcrumbComponent, BannerComponent, LocaleFilterComponent],
  exports: [HeaderComponent, BreadcrumbComponent, BannerComponent, LocaleFilterComponent]
})
export class SharedModule {
}
