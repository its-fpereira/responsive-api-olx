import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocaleFilterComponent } from './locale-filter.component';

describe('LocaleFilterComponent', () => {
  let component: LocaleFilterComponent;
  let fixture: ComponentFixture<LocaleFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocaleFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocaleFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
