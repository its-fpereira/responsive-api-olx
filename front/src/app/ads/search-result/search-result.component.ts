import {Component} from '@angular/core';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent {

  isToggled: boolean = false;
  ads = [{
    imgSrc: './assets/alfaia.png',
    imgAlt: 'Alfaia',
    locale: 'Botafogo, Rio de Janeiro',
    moreInfo: '',
    price: 'R$ 200.909',
    publishDate: 'Hoje às 12:20',
    startingFrom: false,
    title: 'Fun Residencial - 02 quartos c/suíte e Brinde armários no Cachambi - Pronto para morar'
  }, {
    imgSrc: './assets/flauta.png',
    imgAlt: 'Flauta',
    locale: 'Botafogo, Rio de Janeiro',
    moreInfo: '1 quarto | 52 m² | Condomínio: R$ 110',
    price: 'R$ 200.909',
    publishDate: 'Hoje às 12:20',
    startingFrom: false,
    title: 'Fun Residencial - 02 quartos c/suíte'
  }, {
    imgSrc: './assets/violao.png',
    imgAlt: 'Violão',
    locale: 'Botafogo, Rio de Janeiro',
    moreInfo: '65.000 km | Câmbio: automático | Flex - Anúncio profissional',
    price: 'R$ 200.909',
    publishDate: 'Hoje às 12:20',
    startingFrom: false,
    title: 'Fun Residencial - 02 quartos c/suíte e Brinde'
  }, {
    imgSrc: './assets/lira.png',
    imgAlt: 'Lira',
    locale: 'Botafogo, Rio de Janeiro',
    moreInfo: '1 quarto | 52 m² | Condomínio: R$ 110',
    price: 'R$ 92.200.909',
    publishDate: 'Hoje às 12:20',
    startingFrom: true,
    title: 'Fun Residencial - 02 quartos c/suíte e Brinde armários no Cachambi - Pronto para morar'
  }, {
    imgSrc: './assets/alfaia.png',
    imgAlt: 'Alfaia',
    locale: 'Botafogo, Rio de Janeiro',
    moreInfo: '1 quarto | 52 m² | Condomínio: R$ 110',
    price: 'R$ 200.909',
    publishDate: 'Hoje às 12:20',
    startingFrom: true,
    title: 'Fun Residencial - 02 quartos c/suíte e Brinde armários no Cachambi - Pronto para morar'
  }];

  adsMobile = [{
    imgSrc: './assets/lira.png',
    imgAlt: 'Lira',
    locale: '',
    moreInfo: '',
    price: 'R$ 390',
    publishDate: 'Hoje 16:30',
    startingFrom: false,
    title: 'Lira Antiga 22 Cordas Mini Harpa Em Cédr...'
  }, {
    imgSrc: './assets/violino.png',
    imgAlt: 'Violino',
    locale: '',
    moreInfo: '',
    price: 'R$ 1987',
    publishDate: 'Hoje 09:21',
    startingFrom: false,
    title: 'Violino Roma Artesanal'
  }, {
    imgSrc: './assets/alfaia.png',
    imgAlt: 'Alfaia',
    locale: '',
    moreInfo: '',
    price: 'R$ 2504',
    publishDate: 'Hoje 08:16',
    startingFrom: false,
    title: 'Alfaia 23'
  }, {
    imgSrc: './assets/flauta.png',
    imgAlt: 'Flauta',
    locale: '',
    moreInfo: '',
    price: 'R$ 578',
    publishDate: '19 de Ago 23:54',
    startingFrom: false,
    title: 'Flauta Doce Csr Sh1503 Em Dó Ger...'
  }, {
    imgSrc: './assets/violao.png',
    imgAlt: 'Violão',
    locale: '',
    moreInfo: '',
    price: 'R$ 100',
    publishDate: '19 de Ago 23:19',
    startingFrom: false,
    title: 'Vioão Di Giorgio'
  }, {
    imgSrc: './assets/zabumba.png',
    imgAlt: 'Zabumba',
    locale: '',
    moreInfo: '',
    price: 'R$ 850',
    publishDate: '19 de Ago 14:00',
    startingFrom: false,
    title: 'Zabumba 20x20 C/ Maçaneta'
  }, {
    imgSrc: './assets/lira.png',
    imgAlt: 'Lita',
    locale: '',
    moreInfo: '',
    price: 'R$ 15',
    publishDate: '19 de Ago 07:05',
    startingFrom: false,
    title: 'Lira Antiga 22 Cordas Mini Harpa em Cédr...'
  }, {
    imgSrc: './assets/violino.png',
    imgAlt: 'Violino',
    locale: '',
    moreInfo: '',
    price: 'R$ 65',
    publishDate: '18 de Ago 23:59',
    startingFrom: false,
    title: 'Violino Roma Artesanal'
  }, {
    imgSrc: './assets/alfaia.png',
    imgAlt: 'Alfaia',
    locale: '',
    moreInfo: '',
    price: 'R$ 150',
    publishDate: '18 de Ago 22:10',
    startingFrom: false,
    title: 'Alfaia 23 - Morre Congo, Nasce Congo'
  }];

  onToggled(value: any){
    this.isToggled = value;
  }
}
