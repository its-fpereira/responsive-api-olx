import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SugestionsComponent } from './sugestions.component';

describe('SugestionsComponent', () => {
  let component: SugestionsComponent;
  let fixture: ComponentFixture<SugestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SugestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SugestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
