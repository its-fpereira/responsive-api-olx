import {List} from 'immutable';

export interface Item {
  label: string;
  url: string;
  icon: string;
  items: List<Item>;
}
