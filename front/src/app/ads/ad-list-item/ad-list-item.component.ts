import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-ad-list-item',
  templateUrl: './ad-list-item.component.html',
  styleUrls: ['./ad-list-item.component.scss']
})
export class AdListItemComponent {

  @Input() imgSrc: string;
  @Input() imgAlt: string;
  @Input() locale: string;
  @Input() moreInfo: string;
  @Input() price: string;
  @Input() publishDate: string;
  @Input() startingFrom: boolean;
  @Input() title: string;
}
