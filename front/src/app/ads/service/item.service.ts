import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ItemService {

  apiPath = '/item';
  headers: Headers;

  constructor(private _http: Http) {
    this.prepareRequestHeaders();
  }

  getItems() {
    return this._http.get(`${environment.apiBasePath}${this.apiPath}`, {headers: this.headers});
  }

  private prepareRequestHeaders() {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }
}
