import {Injectable} from '@angular/core';
import {List} from 'immutable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ItemService} from '../service/item.service';
import {asObservable} from '../../util/asObservable';
import {Item} from '../item';

@Injectable()
export class ItemStore {

  private _items: BehaviorSubject<List<Item>> = new BehaviorSubject(List([]));

  constructor(private _service: ItemService) {
    this.getItems();
  }

  get items() {
    return asObservable(this._items);
  }

  getItems() {
    this._service.getItems().subscribe(
      res => this._items.next(List(<Item[]>res.json()['items'])),
      err => console.log('Error trying to retrieve data.')
    );
  }
}
