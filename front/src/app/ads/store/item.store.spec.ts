import { TestBed, inject } from '@angular/core/testing';

import { ItemStore } from './item.store';

describe('ItemStore', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemStore]
    });
  });

  it('should be created', inject([ItemStore], (service: ItemStore) => {
    expect(service).toBeTruthy();
  }));
});
