import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SearchResultComponent} from './search-result/search-result.component';
import {SharedModule} from '../shared/shared.module';
import {GalleryComponent} from './gallery/gallery.component';
import {AdListItemComponent} from './ad-list-item/ad-list-item.component';
import {SugestionsComponent} from './sugestions/sugestions.component';
import {CategoryFilterComponent} from './category-filter/desktop/category-filter.component';
import {MobileComponent} from './category-filter/mobile/mobile.component';
import {ItemService} from './service/item.service';
import {ItemStore} from './store/item.store';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    SharedModule
  ],
  declarations: [SearchResultComponent, GalleryComponent, AdListItemComponent, SugestionsComponent, CategoryFilterComponent, MobileComponent],
  exports: [SearchResultComponent],
  providers: [
    ItemService,
    ItemStore
  ]
})
export class AdsModule {
}
