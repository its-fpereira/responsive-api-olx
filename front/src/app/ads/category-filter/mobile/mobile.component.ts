import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {List} from 'immutable';
import {Item} from '../../item';
import {ItemStore} from '../../store/item.store';

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss']
})
export class MobileComponent implements OnInit {

  @Input() isToggled: boolean;
  @Output() onToggled = new EventEmitter();
  menuToggled: string;
  categoryActive: Item;
  items: List<Item>;

  constructor(private _itemStore: ItemStore) {
  }

  ngOnInit(): void {
    this.menuToggled = '';
    this.getAllCategories();
  }

  changeMenuToggled(label: string) {
    if (this.menuToggled === '') {
      this.menuToggled = label;
      this.categoryActive = this.items.filter(category => category.label === label).get(0);
      this.items = this.categoryActive.items;
    }
  }

  backFromSub() {
    this.categoryActive = null;
    this.menuToggled = '';
    this.getAllCategories();
  }

  getAllCategories() {
    this._itemStore.items.subscribe(x => {
      this.items = <List<Item>>x;
    });
  }

  leaveCategoriesFilter() {
    this.isToggled = !this.isToggled;
    this.onToggled.next(this.isToggled);
  }
}
