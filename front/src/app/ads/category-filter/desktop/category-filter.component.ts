import {Component, OnInit} from '@angular/core';
import {List} from 'immutable';
import {ItemStore} from '../../store/item.store';
import {Item} from '../../item';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.scss']
})
export class CategoryFilterComponent implements OnInit {
  items: List<Item>;
  menuCollapsed: string;

  constructor(private _itemStore: ItemStore) {
  }

  ngOnInit(): void {
    this._itemStore.items.subscribe(x => {
      this.items = <List<Item>>x;
    });
  }

  changeMenuCollapsed(label: string) {
    if (this.menuCollapsed == label) this.menuCollapsed = '';
    else this.menuCollapsed = label;
  }
}
