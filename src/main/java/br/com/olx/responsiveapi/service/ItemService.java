package br.com.olx.responsiveapi.service;

import br.com.olx.responsiveapi.model.Item;
import br.com.olx.responsiveapi.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    private final ItemRepository repository;

    @Autowired
    public ItemService(ItemRepository repository) {
        this.repository = repository;
    }

    public void deleteItem(Item item) {
        repository.delete(item);
    }

    public void insertItem(Item item) {
        repository.save(item);
    }

    public List<Item> getItems() {
        return repository.buscarItems();
    }

    public Item getItem(Long id) {
        return repository.findOne(id);
    }

    public Item updateItem(Item item) {
        return repository.save(item);
    }
}
