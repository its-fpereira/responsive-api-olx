package br.com.olx.responsiveapi.controller.wrapper;

import br.com.olx.responsiveapi.model.Item;

import java.io.Serializable;
import java.util.List;

public class ItemWrapper implements Serializable {

    private static final long serialVersionUID = 8661047536845725955L;

    public ItemWrapper(List<Item> items) {
        this.items = items;
    }

    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
