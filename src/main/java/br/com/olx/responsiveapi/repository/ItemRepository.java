package br.com.olx.responsiveapi.repository;

import br.com.olx.responsiveapi.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public interface ItemRepository extends JpaRepository<Item, Long>, JpaSpecificationExecutor {

    @Query("select item from Item item where item.parent is null")
    List<Item> buscarItems();

}
