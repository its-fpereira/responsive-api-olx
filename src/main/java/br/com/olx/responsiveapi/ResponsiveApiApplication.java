package br.com.olx.responsiveapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan()
@EnableJpaRepositories(basePackages = "br.com.olx.responsiveapi.repository")
@EntityScan(basePackages = "br.com.olx.responsiveapi.model")
@SpringBootApplication(scanBasePackages = "br.com.olx.responsiveapi")
public class ResponsiveApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ResponsiveApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ResponsiveApiApplication.class);
    }
}
